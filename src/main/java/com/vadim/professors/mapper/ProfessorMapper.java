package com.vadim.professors.mapper;
import com.vadim.professors.entity.Professor;
import com.vadim.professors.entity.dto.ProfessorDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProfessorMapper {

    List<ProfessorDto> map(List<Professor> professor);

    ProfessorDto map(Professor professor);

    Professor map(ProfessorDto dto);
}
