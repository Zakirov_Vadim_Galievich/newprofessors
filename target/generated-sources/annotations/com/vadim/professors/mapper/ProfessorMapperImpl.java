package com.vadim.professors.mapper;

import com.vadim.professors.entity.Course;
import com.vadim.professors.entity.Professor;
import com.vadim.professors.entity.dto.CourseDto;
import com.vadim.professors.entity.dto.ProfessorDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-01-12T23:24:51+0500",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (BellSoft)"
)
@Component
public class ProfessorMapperImpl implements ProfessorMapper {

    @Override
    public List<ProfessorDto> map(List<Professor> professor) {
        if ( professor == null ) {
            return null;
        }

        List<ProfessorDto> list = new ArrayList<ProfessorDto>( professor.size() );
        for ( Professor professor1 : professor ) {
            list.add( map( professor1 ) );
        }

        return list;
    }

    @Override
    public ProfessorDto map(Professor professor) {
        if ( professor == null ) {
            return null;
        }

        ProfessorDto professorDto = new ProfessorDto();

        professorDto.setId( professor.getId() );
        professorDto.setFirstname( professor.getFirstname() );
        professorDto.setLastname( professor.getLastname() );
        professorDto.setHours( professor.getHours() );
        professorDto.setBaserate( professor.getBaserate() );
        professorDto.setSalary( professor.getSalary() );
        professorDto.setSnils( professor.getSnils() );
        professorDto.setCourses( courseListToCourseDtoList( professor.getCourses() ) );

        return professorDto;
    }

    @Override
    public Professor map(ProfessorDto dto) {
        if ( dto == null ) {
            return null;
        }

        Professor professor = new Professor();

        professor.setCourses( courseDtoListToCourseList( dto.getCourses() ) );
        professor.setId( dto.getId() );
        professor.setFirstname( dto.getFirstname() );
        professor.setLastname( dto.getLastname() );
        professor.setHours( dto.getHours() );
        professor.setBaserate( dto.getBaserate() );
        professor.setSalary( dto.getSalary() );
        professor.setSnils( dto.getSnils() );

        return professor;
    }

    protected CourseDto courseToCourseDto(Course course) {
        if ( course == null ) {
            return null;
        }

        CourseDto courseDto = new CourseDto();

        courseDto.setName( course.getName() );
        courseDto.setCourse( course.getCourse() );
        courseDto.setIdentifire( course.getIdentifire() );

        return courseDto;
    }

    protected List<CourseDto> courseListToCourseDtoList(List<Course> list) {
        if ( list == null ) {
            return null;
        }

        List<CourseDto> list1 = new ArrayList<CourseDto>( list.size() );
        for ( Course course : list ) {
            list1.add( courseToCourseDto( course ) );
        }

        return list1;
    }

    protected Course courseDtoToCourse(CourseDto courseDto) {
        if ( courseDto == null ) {
            return null;
        }

        Course course = new Course();

        course.setName( courseDto.getName() );
        course.setCourse( courseDto.getCourse() );
        course.setIdentifire( courseDto.getIdentifire() );

        return course;
    }

    protected List<Course> courseDtoListToCourseList(List<CourseDto> list) {
        if ( list == null ) {
            return null;
        }

        List<Course> list1 = new ArrayList<Course>( list.size() );
        for ( CourseDto courseDto : list ) {
            list1.add( courseDtoToCourse( courseDto ) );
        }

        return list1;
    }
}
