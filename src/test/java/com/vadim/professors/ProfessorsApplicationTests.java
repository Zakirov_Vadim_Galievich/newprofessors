package com.vadim.professors;

import com.vadim.professors.entity.Professor;
import com.vadim.professors.repository.ProfessorRepository;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;


@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ProfessorsApplicationTests {

    public static final Professor PROFESSOR = new Professor("Vadim", "Zakirov", 40, 1000, 123456789);

    @Autowired
    private ProfessorRepository repository;
//
//    @Container
//    public static PostgreSQLContainer container = new PostgreSQLContainer("postgres:14.1")
//            .withUsername("postgres")
//            .withPassword("postgres")
//            .withDatabaseName("test");



    @Container
    public static PostgreSQLContainer postgreSQLContainer = (PostgreSQLContainer) new PostgreSQLContainer("postgres:11.1")

            .withUsername("sa")
            .withPassword("sa");


    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
    }


//    static class Initializer
//            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
//        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
//            TestPropertyValues.of(
//                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
//                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
//                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
//            ).applyTo(configurableApplicationContext.getEnvironment());
//        }
//    }

    @Test
//    @Sql("stubs-data.sql")
    @Transactional
    void givenProfessorBySnils_ThenGetProfessor() {
        repository.save(PROFESSOR);
    }

//    @Test
//    @Sql("stubs-data.sql")
//    void givenProfessorBySnils_ThenGetProfessor() {
//        repository.save(PROFESSOR);
//        System.out.println("Context load");
////        MvcResult result = mockMvc.perform(get("/api/professor/169856354"))
////                .andExpect(status().isOk())
////                .andExpect(MockMvcResultMatchers.content().json(responseBody))
////                .andDo(print())
////                .andReturn();
//    }
}
