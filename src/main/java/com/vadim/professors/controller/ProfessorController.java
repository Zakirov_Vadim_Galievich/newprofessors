package com.vadim.professors.controller;

import com.vadim.professors.entity.Professor;
import com.vadim.professors.entity.dto.ProfessorDto;
import com.vadim.professors.exception.DublicateEntityException;
import com.vadim.professors.mapper.ProfessorMapper;
import com.vadim.professors.service.ProfessorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/professor")
@RequiredArgsConstructor
public class ProfessorController {

    private final ProfessorService professorService;
    private final ProfessorMapper mapper;

    //************************************************************************************************
    //Профессоры
    //**********************************************************************************************

    //Получение всех записей профессоров
    @GetMapping(value = "")
    public List<ProfessorDto> findAllProf() {
        return professorService.getAllProfessors();
    }

    //Получение по СНИЛС
    @GetMapping(value = "/{snils}")
    @Operation(description = "Получение информации о профессоре по СНИЛС", responses = {@ApiResponse(responseCode = "200", description = "Данные о профессоре получены"),
            @ApiResponse(responseCode = "404", description = "Данный профессор не найден")})
    public ResponseEntity<ProfessorDto> readProf(@PathVariable(name = "snils")
                                                 @Parameter(description = "СНИЛС профессора") Long snils) {
        ProfessorDto professor = professorService.findBySnils(snils);
        return professor != null
                ? new ResponseEntity<>(professor, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Создание
    @PostMapping(value = "")
    @Operation(description = "Создание профессора", responses = {@ApiResponse(responseCode = "200", description = "Профессор добавлен")})
    public ResponseEntity<?> createProf(@RequestBody ProfessorDto newProfessor) {
            professorService.save(newProfessor);
            return new ResponseEntity<>(HttpStatus.OK);
    }

    //Обновление
    @PutMapping(value = "")
    @Operation(description = "Обновление данных у имеющегося профессора", responses = {@ApiResponse(responseCode = "200", description = "Данные профессора обновлены")})
    public ResponseEntity<?> updateProf(@RequestBody Professor professor) {
        boolean result = professorService.update(professor);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //Удаление
    @DeleteMapping(value = "/{snils}")
    @Operation(description = "Удаление профессора", responses = {@ApiResponse(responseCode = "200", description = "Удаление профессора прошло успешно"),
            @ApiResponse(responseCode = "404", description = "Удаление профессора не произошло, профессор не найден")})
    public ResponseEntity<?> deleteProf(@PathVariable(name = "snils") Long snils) {
        final boolean deleted = professorService.delete(snils);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
