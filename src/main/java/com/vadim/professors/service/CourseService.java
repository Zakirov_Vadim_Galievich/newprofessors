package com.vadim.professors.service;

import com.vadim.professors.entity.Course;
import com.vadim.professors.entity.dto.CourseDto;
import com.vadim.professors.exception.DublicateEntityException;
import com.vadim.professors.exception.NoEntityException;
import com.vadim.professors.mapper.CourseMapper;
import com.vadim.professors.repository.CourseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CourseService {
    private final CourseRepository courseRepository;
    private final CourseMapper courseMapper;

 //Создание курса отдельно от преподавателя не предполагается
    public Course save(CourseDto course) {
        var course1 = courseRepository.findByIdentifire(course.getIdentifire());
        if (course1.isPresent()) {
            throw new DublicateEntityException("Курс с идентификатором " + course.getIdentifire() + " уже существует");
        } else {
            return courseRepository.save(courseMapper.map(course));
        }
    }

    public boolean update(Course course) {
        Course currentCourse = courseRepository.findByIdentifire(course.getIdentifire()).get();
        if (currentCourse != null) {
            course.setId(currentCourse.getId());
            courseRepository.save(course);
            return true;
        } else {
            return false;
        }
    }

    public boolean delete(Long ident) {
        Course course = courseRepository.findByIdentifire(ident).orElseThrow(() -> new NoEntityException(ident));
        courseRepository.deleteById(course.getId());
        return true;
    }

    public CourseDto findByIdent(Long ident) {
        return courseMapper.map(courseRepository.findByIdentifire(ident).orElseThrow(() -> new NoEntityException(ident)));
    }

    public List<CourseDto> findAll() {
        return courseMapper.map(courseRepository.findAll());
    }

    public List<CourseDto> findCoursesByProf(Long professorId) {
        List<Long> listWithCourseId = courseRepository.findCourseIdByProfessor(professorId).orElseThrow(() -> new NoEntityException(professorId));
       return courseMapper.map((courseRepository.findCourseByProfessor(listWithCourseId).orElseThrow(() -> new NoEntityException(professorId))));
    }
}
