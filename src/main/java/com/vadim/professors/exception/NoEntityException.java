package com.vadim.professors.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NoEntityException extends RuntimeException{
    public NoEntityException(long snils) {
        super("The attribute with number:" + snils + " is not exist");
        log.error("The attribute with number:" + snils + " is not exist");
    }
}
