package com.vadim.professors.service;

import com.vadim.professors.entity.Professor;
import com.vadim.professors.entity.dto.ProfessorDto;
import com.vadim.professors.exception.DublicateEntityException;
import com.vadim.professors.exception.NoEntityException;
import com.vadim.professors.mapper.ProfessorMapper;
import com.vadim.professors.repository.ProfessorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class ProfessorService {
    private final ProfessorRepository repository;
    private final ProfessorMapper mapper;

    public List<ProfessorDto> getAllProfessors() {
        return mapper.map(repository.findAll());
    }

    public Professor save(ProfessorDto professorDto) {
        Professor professor = mapper.map(professorDto);
        boolean present = repository.findBySnils(professor.getSnils()).isPresent();
        if (!present) {
            professor.setSalary();
            return repository.save(professor);
        } else {
            throw new DublicateEntityException(String.valueOf(professor.getSnils()));
        }
    }

    public Optional<Professor> findPersonById(Long id) {
        return repository.findById(id);
    }


    public boolean update(Professor newProfessor) {
        Professor currentProfessor = repository.findBySnils(newProfessor.getSnils()).get();
        if(currentProfessor != null) {
            Long id = currentProfessor.getId();
            newProfessor.setId(id);
            newProfessor.setCourses(currentProfessor.getCourses());
            newProfessor.setSalary();
            repository.save(newProfessor);
            return true;
        } else {
            return false;
        }
    }

    public boolean delete(Long snils) {
        Professor professor = repository.findBySnils(snils).orElseThrow(() -> new NoEntityException(snils));
        repository.deleteById(professor.getId());
        return true;
    }

    public ProfessorDto findBySnils(Long snils) {
        return mapper.map(repository.findBySnils(snils).orElseThrow(() -> new NoEntityException(snils)));
    }


}
