package com.vadim.professors.entity.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vadim.professors.entity.Course;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
public class ProfessorDto {
    @JsonIgnore
    private Long id;
    private String firstname;
    private String lastname;
    private int hours;
    private int baserate;
    @JsonIgnore
    private double salary;
    private long snils;
    //@JsonManagedReference
    private List<CourseDto> courses;

    @Override
    public String toString() {
        return "ProfessorDto{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", hours=" + hours +
                ", baserate=" + baserate +
                ", salary=" + salary +
                ", snils=" + snils +
                '}';
    }
}
