package com.vadim.professors.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.EAGER;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "professors")
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "hours")
    private int hours;

    @Column(name = "baserate")
    private int baserate;

    @Column(name = "salary")
    @JsonIgnore
    private double salary;

    @Column(name = "snils")
    private long snils;

    @OneToMany(fetch = EAGER, cascade = {PERSIST, MERGE}, orphanRemoval = true, mappedBy = "professor")
    @JsonManagedReference
    private List<Course> courses;

    public Professor(String firstname, String lastname, int hours, int baserate, long snils) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.hours = hours;
        this.baserate = baserate;
        this.snils = snils;
    }

    public void addCoursesToPerson(Course course) {
        if(courses == null) {
            courses = new ArrayList<>();
        }
        courses.add(course);
        course.setProfessor(this);
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSalary() {
        double coficient = (courses.size() * 0.1) + 1;
        double preSalary = ((hours * 3.0 * baserate) / 4) * coficient;
        String preResult = String.format("%.2f", preSalary);
        String result = preResult.replace(',', '.');
        salary = Double.parseDouble(result);

    }

    @Override
    public String toString() {
        return "Professor{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", hours=" + hours +
                ", baserate=" + baserate +
                ", salary=" + salary +
                ", snils=" + snils +
                '}';
    }
}