package com.vadim.professors.controller;

import com.vadim.professors.entity.Course;
import com.vadim.professors.entity.dto.CourseDto;
import com.vadim.professors.entity.dto.ProfessorDto;
import com.vadim.professors.service.CourseService;
import com.vadim.professors.service.ProfessorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/courses")
@Slf4j
@RequiredArgsConstructor
public class CourseController {
    private final CourseService courseService;
    private final ProfessorService professorService;

    //************************************************************************************************
    //Курсы
    //**********************************************************************************************

    //Получение всех курсов
    @GetMapping(value = "")
    public List<CourseDto> findAllCourse() {
        return courseService.findAll();
    }

    //Получение по идентификатору
    @Operation(description = "Получение курсу по идентификатору", responses = {@ApiResponse(responseCode = "200", description = "Получение курса"),
            @ApiResponse(responseCode = "404", description = "Курс не найден")})
    @GetMapping(value = "/{ident}")
    public ResponseEntity<CourseDto> readCourse(@PathVariable(name = "ident")
                                                @Parameter(description = "Уникальный идентификатор курса") Long ident) {
        CourseDto dto = courseService.findByIdent(ident);
        return dto != null
                ? new ResponseEntity<>(dto, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //Получение всех курсов у конкретного преподавателя
    @GetMapping(value = "/coursesByProf/{snils}")
    public List<CourseDto> getCoursesByPerson(@PathVariable(name = "snils") Long snils) {
        Long professorId = professorService.findBySnils(snils).getId();
        return courseService.findCoursesByProf(professorId);
    }

    //Добавление курса
    @PostMapping(value = "")
    @Operation(description = "Добавление нового курса", responses = {@ApiResponse(responseCode = "200", description = "Курс добавлен")})
    public ResponseEntity<?> create(@RequestBody CourseDto course) {
        courseService.save(course);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //Обновление
    @PutMapping(value = "")
    @Operation(description = "Обновление имеющегося курса", responses = {@ApiResponse(responseCode = "200", description = "Обновление прошло успешно"),
            @ApiResponse(responseCode = "304", description = "Обновление курса не произошло")})
    public ResponseEntity<?> update(@RequestBody
                                    @Parameter(description = "Курс для удаления") Course course) {
        final boolean updated = courseService.update(course);
        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    //Удаление
    @DeleteMapping(value = "/{ident}")
    @Operation(description = "Удаление имеющегося курса", responses = {@ApiResponse(responseCode = "200", description = "Удаление прошло успешно"),
            @ApiResponse(responseCode = "304", description = "Удаление курса не произошло, курс не найден")})
    public ResponseEntity<?> delete(@PathVariable(name = "ident")
                                    @Parameter(description = "Уникальный идентификатор курса") Long ident) {
        boolean deleted = courseService.delete(ident);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}