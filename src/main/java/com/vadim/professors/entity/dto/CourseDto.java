package com.vadim.professors.entity.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vadim.professors.entity.Professor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CourseDto {
    private String name;
    private String course;
    private Long identifire;


    @Override
    public String toString() {
        return "CourseDto{" +
                "name='" + name + '\'' +
                ", course='" + course + '\'' +
                ", identifire=" + identifire +
                '}';
    }
}
