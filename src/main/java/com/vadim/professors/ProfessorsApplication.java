package com.vadim.professors;

import com.vadim.professors.service.CourseService;
import com.vadim.professors.service.ProfessorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class ProfessorsApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(ProfessorsApplication.class, args);
    }
    private final CourseService courseService;
    private final ProfessorService professorService;
    @Override
    public void run(String... args) throws Exception {
    }

}
