package com.vadim.professors.repository;

import com.vadim.professors.entity.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Component
public interface ProfessorRepository extends JpaRepository<Professor, Long> {
     @Query(value = "SELECT * FROM professors.professors WHERE snils= ?", nativeQuery = true)
     Optional<Professor> findBySnils(Long snils);

}
