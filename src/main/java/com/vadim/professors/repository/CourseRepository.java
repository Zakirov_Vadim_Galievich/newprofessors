package com.vadim.professors.repository;

import com.vadim.professors.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    @Query("SELECT f FROM Course f WHERE f.identifire= ?1")
    Optional<Course> findByIdentifire(Long identifier);

    @Query("SELECT f FROM Course f WHERE f.id IN :ids")
    Optional<List<Course>> findCourseByProfessor(@Param("ids") List<Long> listWithCourseId);

    @Query(value = "SELECT course_id FROM prof_course WHERE prof_id = ?1", nativeQuery = true)
    Optional<List<Long>> findCourseIdByProfessor(Long professorId);

}
