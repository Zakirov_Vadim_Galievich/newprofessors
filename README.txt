CRUD программа для работы с преподавателями и прикрепленными к ним курсами, работающая на основе RESTful.
Программа состоит из компонента базы данных и сервисного приложения по приемы и отправки JSON файлов по следующим методам:

1. Блок профессоров

1.1 GET - "/api/professors" запрос по получению всего списка имеющихся преподавателей и прикрепленных к ним курсам.

1.2. PUT - "/api/professors" запрос на обновление данных о преподавателе, формат запроса:
{
  "firstname": "string",
  "lastname": "string",
  "hours": 0,
  "baserate": 0,
  "snils": 0,
  "courses": [
    {
      "name": "string",
      "course": "string",
      "identifire": 0
    }
  ]
}
(данные поля courses опускаются)

1.3. POST - "/api/professors" запрос на добавление нового преподавателя и(или) прикрепляемого к нему курса(сов), формат запроса:
{
  "firstname": "string",
  "lastname": "string",
  "hours": 0,
  "baserate": 0,
  "snils": 0,
  "courses": [
    {
      "name": "string",
      "course": "string",
      "identifire": 0
    }
  ]
}
1.4. GET - "api/professor/{snils}" - запрос для получения конкретного преподователя по уникальному идентификатору snils.

1.5. DELETE - "api/professors/{snils}" - запрос для удаления преподавателя из базы данных
по уникальному идентификатору snils, прикрепленные курсы не удаляются.

2. Блок курсов.

2.1. GET - "/api/courses" - получение всех имеющихся курсов в отрыве от преподавателя.

2.2. GET - "/courses/{ident}" - запрос на получение конкретного курса по идентификатору.

2.3. GET - "/coursesByProf/{snils}" - получение всех курсов у конкретного преподавателя, определяемого по snils.

2.4. PUT - "/courses" - запрос на обновление данных о курсе - не работает

2.5. DELETE - "/courses/{ident}" - не работает.

При создании профессора и добавлении его в систему,последняя автоматически высчитывает значения его заработной платы с учетом
нагрузки по количеству курсов и ставки за внесенное количество часов.

3. Команды для создания таблиц базы данных.
CREATE DATABASE study

CREATE TABLE professors
(
    id SERIAL PRIMARY KEY,
    firstName CHARACTER VARYING(30),
    lastName CHARACTER VARYING(30),
    hours INTEGER,
    baserate INTEGER,
    salary DECIMAL,
    snils INTEGER
);

CREATE TABLE courses
(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(30),
    course CHARACTER VARYING(30),
    identifier INTEGER
);

CREATE TABLE prof_course
(
    id SERIAL PRIMARY KEY,
    prof_id INTEGER,
    course_id INTEGER,
    FOREIGN KEY (prof_id) REFERENCES professors (id),
    FOREIGN KEY (course_id) REFERENCES courses (id)
);

ALTER TABLE prof_course ADD CONSTRAINT prof_course_prof_id_course_id UNIQUE (prof_id, course_id);

4. Свойства в application.properties:
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQLDialect
spring.datasource.url=jdbc:postgresql://localhost:5433/postgres
spring.datasource.username=postgres
spring.datasource.password=postgres
spring.jpa.generate-ddl=true
server.port=8081

5. Свойства docker-compose для создания контейнерав БД.
version: '3.5'
services:
  postgres:
    container_name: postgres_container
    image: postgres
    environment:
      POSTGRES_USER: ${POSTGRES_USER:-postgres}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-changeme}
      PGDATA: /data/postgres
    volumes:
      - postgres:/data/postgres
    ports:
      - "5432:5432"
    networks:
      - postgres
    restart: unless-stopped

