package com.vadim.professors.mapper;

import com.vadim.professors.entity.Course;
import com.vadim.professors.entity.dto.CourseDto;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface CourseMapper {
    List<CourseDto> map(List<Course> course);

    CourseDto map(Course course);

    Course map(CourseDto dto);
}
