package com.vadim.professors.mapper;

import com.vadim.professors.entity.Course;
import com.vadim.professors.entity.dto.CourseDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-01-12T23:24:50+0500",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (BellSoft)"
)
@Component
public class CourseMapperImpl implements CourseMapper {

    @Override
    public List<CourseDto> map(List<Course> course) {
        if ( course == null ) {
            return null;
        }

        List<CourseDto> list = new ArrayList<CourseDto>( course.size() );
        for ( Course course1 : course ) {
            list.add( map( course1 ) );
        }

        return list;
    }

    @Override
    public CourseDto map(Course course) {
        if ( course == null ) {
            return null;
        }

        CourseDto courseDto = new CourseDto();

        courseDto.setName( course.getName() );
        courseDto.setCourse( course.getCourse() );
        courseDto.setIdentifire( course.getIdentifire() );

        return courseDto;
    }

    @Override
    public Course map(CourseDto dto) {
        if ( dto == null ) {
            return null;
        }

        Course course = new Course();

        course.setName( dto.getName() );
        course.setCourse( dto.getCourse() );
        course.setIdentifire( dto.getIdentifire() );

        return course;
    }
}
