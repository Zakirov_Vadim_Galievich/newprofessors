package com.vadim.professors.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DublicateEntityException extends RuntimeException {
    public DublicateEntityException(String message) {
        super("Добавляемый элемент с идентификатором: " + message + " уже существует");
        log.error("Добавляемый элемент с идентификатором: " + message + " уже существует");
    }
}